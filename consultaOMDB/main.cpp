#include <QCoreApplication>
#include <QtCore/QUrl>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>

void printCommandList(){
    qDebug() << "Lista de comandos disponiveis:" << endl;
    qDebug() << "-h \t--help" << endl;
    qDebug() << "-s \t--search   Busca pelo título da media" << endl;
    qDebug() << "-i \t--imdb     Busca pelo codigo IMDB que seja valido" << endl;
    qDebug() << "-t \t--type     Retorna o tipo media selecionado. (movie, series, episode)" << endl;
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QString sRequest ="http://omdbapi.com/?apikey=4abb576a";

    if(app.arguments().size() > 2){
        int countCommands = 0;
        for(int i =0;i < app.arguments().size(); i++){
            if(app.arguments().at(i).contains("-")){countCommands++;}
        }
        if(countCommands > 1){
            qDebug().noquote() << "ERRO - apenas um comando por vez!" << endl;
            printCommandList();
            return 0;
        }
        if(app.arguments().at(1) == "-h" || app.arguments().at(1) == "--help"){
            printCommandList();
            return 0;
        }

        if(app.arguments().at(1) == "-i" || app.arguments().at(1) == "--imdb"){
            sRequest += "&i=";
        }
        else{
            sRequest += "&t=";
        }

        for(int i =2;i < app.arguments().size(); i++){
            sRequest += app.arguments().at(i) + "+";
        }

        QEventLoop eventLoop;
        QNetworkAccessManager mgr;
        QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
        QNetworkRequest req( sRequest);
        QNetworkReply *reply = mgr.get(req);
        eventLoop.exec();

        if (reply->error() == QNetworkReply::NoError) {
            QString strReply = (QString)reply->readAll();
            QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
            QJsonObject jsonObj = jsonResponse.object();

                if(jsonResponse.isObject()){
                    QJsonObject valuesO = jsonResponse.object();
                    if(app.arguments().at(1) == "-t" || app.arguments().at(1) == "--type"){
                        qDebug().noquote() << valuesO["Type"].toString();
                    }
                    else{
                        for(auto key: valuesO.keys()){
                            qDebug().noquote()<<key<<": \t"<<valuesO[key].toString()<<endl;
                        }
                    }
                }

            delete reply;
        }
        else {
            qDebug() << "Erro :(" << endl <<reply->errorString();
            delete reply;
        }
    }
    else{
        printCommandList();
    }

    return 0;
}



